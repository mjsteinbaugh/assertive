## Release Summary

Incorporates latest exports from assertive.base, assertive.properties, assertive.types, assertive.reflection, assertive.sets.

## Test Environments

* Local macOS Sierra, R-devel 
* Semaphore CI + Ubuntu 14.04, R-devel and R-release
* AppVeyor + Windows Server 2012, R-devel

## R CMD check results

There were no ERRORs or WARNINGs.

## Downstream dependencies

No breaking changes.
